const Joi = require("joi");

exports.validate=(req,res,next)=>{
  const schema =Joi.object({
    email:Joi.array().required().allow().items(Joi.string().email().required()),
    message:Joi.string().required()
  });
  const result=schema.validate(req.body);
  //console.log(JSON.stringify(result));
  if(result.error){
    res.status(400).send(result.error.details[0].message);
    return;
  }
  console.log("moving out of middleware");
  next();
}