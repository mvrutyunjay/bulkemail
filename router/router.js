const express=require('express');
const router=express.Router();
const {message}=require('../controller/controller');
const {validate}=require('../validator/validate');

router.post('/em',validate,message);


module.exports=router;