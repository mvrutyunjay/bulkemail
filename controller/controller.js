const nodemailer = require('nodemailer');
require("dotenv").config();
let uemail=process.env.Email;
let password=process.env.Password;
var transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: uemail,
    pass: password
  }
});
exports.message=(req,reply)=>{
    const {email,message}=req.body;
        var mailOptions = {
            from: uemail,
            to: email,
            subject: 'Sending Email using Node.js',
            text: 'Your message!',
            html:'<h1>This is your message</h1>'+message+'<h3>Thankyou</h3>',
          };
          
          transporter.sendMail(mailOptions, function(error, info){
            if (error) {
              console.log(error);
            } else {
              console.log('Email sent: ' + info.response);
              reply.send({ status: '0' ,message:'Email send successfully'});
            }
          });

}