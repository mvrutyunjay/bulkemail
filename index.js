const app=require('./server/server');

const port=process.env.PORT||8080;

app.listen(port,()=>console.log("App is running on port:",port));